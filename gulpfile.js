// /////////////////////////////////////////
// Required
// /////////////////////////////////////////
var gulp 			= require('gulp'),
	uglify 			= require('gulp-uglify'),
	browserSync 	= require('browser-sync').create(),
	sass 			= require('gulp-sass'),
	concat 			= require('gulp-concat'),
	plumber 		= require('gulp-plumber'),
	autoprefixer 	= require('gulp-autoprefixer'),
	del 			= require('del');


// /////////////////////////////////////////
// Configuration
// /////////////////////////////////////////

var wp_theme_name 		= 'theme/';
var broser_sync_proxy 	= 'local.wordpress-starter-pack';
var broser_sync_port 	= 3000;
var theme_location 		= 'public/wp-content/themes/'+wp_theme_name;

var config = {
	js_concat_files: [
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
		'bower_components/jquery_lazyload/jquery.lazyload.js',
		'bower_components/dist/src/js/owl.carousel.min.js',
		'bower_components/wow/dist/wow.min.js',
		theme_location+'src/js/script.js'
	],
	css_concat_files: [
		theme_location+'src/scss/style-template.scss',
		// theme_location+'src/scss/owl-carousel-2/owl.carousel.scss',
		// theme_location+'src/scss/owl-carousel-2/owl.theme.default.scss',
		theme_location+'bower_components/wow/css/libs/animate.css',
		theme_location+'src/scss/lightgallery.css',
		theme_location+'src/scss/style.scss'
	],
	build_files_folder_remove: [
		'build/wp-content/themes/custom-theme/src/scss/',
		'build/wp-content/themes/custom-theme/src/',
		'build/wp-content/themes/custom-theme/dist/js/!(*.min.js)',
		'build/wp-content/themes/custom-theme/dist/css/modules/',
		'build/bower_components',
		'build/wp-content/themes/twenty*',
		'build/wp-content/upgrade'
	],
	js_filename: 'script.min.js',
	css_filename: 'style.css',
	css_destination: theme_location,
	js_destination: theme_location+'dist/js/'
}


// /////////////////////////////////////////
// Scripts Tasks
// /////////////////////////////////////////
gulp.task('scripts', function(){
	gulp.src(theme_location+'src/js/**/*.js')
		.pipe(plumber())
		.pipe(uglify())
		.pipe(concat(config.js_filename))
		.pipe(gulp.dest(config.js_destination))
		.pipe(browserSync.stream());
});



// /////////////////////////////////////////
// Sass Tasks
// /////////////////////////////////////////
gulp.task('sass', function(){
	gulp.src(config.css_concat_files)
		.pipe(plumber())
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(autoprefixer({
			browsers: [
				"Android 2.3",
				"Android >= 4",
				"Chrome >= 20",
				"Firefox >= 24",
				"Explorer >= 8",
				"iOS >= 6",
				"Opera >= 12",
				"Safari >= 6"
			],
			cascade: false
		}))
		.pipe(concat(config.css_filename))
		.pipe(gulp.dest(config.css_destination))
		.pipe(browserSync.stream());
});


// /////////////////////////////////////////
// Build Tasks
// /////////////////////////////////////////

// clear out all files and folders from build folder
gulp.task('build:remove', function(){
	return del([
		'build/**'
	]);
});

// task to create build directory for all files
gulp.task('build:copy', ['build:remove'], function(){
	return gulp.src('app/**/*/')
	.pipe(gulp.dest('build/'));
});

// task to remove unwanted build files
// list all files and directories here that you don't want to include
gulp.task('build:clean', ['build:copy'], function(){
	del(config.build_files_folder_remove);
});

// build task sequence
gulp.task('build', ['build:remove', 'build:copy', 'build:clean']);


// /////////////////////////////////////////
// Browser-Sync Tasks
// /////////////////////////////////////////
gulp.task('browser-sync', function(){
	browserSync.init({
		host: 'local.wordpress-starter-pack',
		proxy: 'local.wordpress-starter-pack',
		port: 3000,
		open: false
	})
	// browserSync.init({
	// 	proxy: "soundplanning.dev"
	// });
});


// /////////////////////////////////////////
// Watch Tasks
// /////////////////////////////////////////
gulp.task('watch', function(){
	gulp.watch(theme_location+'src/js/**/*.js', ['scripts']);
	gulp.watch(theme_location+'src/scss/**/*.scss', ['sass']);
	gulp.watch(theme_location+'src/css/**/*.css', ['sass']);
});


// /////////////////////////////////////////
// Default Tasks
// /////////////////////////////////////////
gulp.task('default', ['sass', 'scripts', 'browser-sync', 'watch']);