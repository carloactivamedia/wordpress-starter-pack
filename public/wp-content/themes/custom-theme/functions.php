<?php 

/************************************************************************************************************************************************************************************
* 																										 	   																		*
*  																			 FRONT END CUSTOM FUNCTIONS 	   																		*
* 																										 	   																		*
*************************************************************************************************************************************************************************************/

require_once('includes/wp_bootstrap_navwalker.php');


register_nav_menus(array(
	'primary' 		=> __( 'Primary Menu', 'asus' ),
	'footer_links'  => __( 'Footer Links', 'asus' )
));


function current_to_active($classes) {
	$classes = str_replace('current-menu-item', 'current-menu-item active', $classes);
	return $classes;
}
add_filter('wp_nav_menu', 'current_to_active');


function frontend_scripts_and_styles(){
	wp_register_script('script', get_template_directory_uri().'/dist/js/script.min.js', array(), '1.0.0', true);
	wp_enqueue_script('script');
	
	wp_register_style('styles', get_stylesheet_uri(), array(), '1.0.0');
	wp_enqueue_style('styles');

	if(!is_admin()) {
		wp_deregister_script('jquery');
		wp_deregister_script('wp-embed');
	}

	wp_localize_script('script', 'wpajax', array(
		'ajax_url' 	 => admin_url('admin-ajax.php'), 
		'ajax_nonce' => wp_create_nonce('wp_nonce')
	));
}
add_action('wp_enqueue_scripts', 'frontend_scripts_and_styles' );


function base_pagination() {
	global $wp_query;

	$big = 999999999; // This needs to be an unlikely integer

	// For more options and info view the docs for paginate_links()
	// http://codex.wordpress.org/Function_Reference/paginate_links
	$paginate_links = paginate_links( array(
		'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'mid_size' => 5
	) );

	// Display the pagination if more than one page is found
	if ( $paginate_links ) {
		echo '<div class="pagination">';
		echo $paginate_links;
		echo '</div><!--// end .pagination -->';
	}
}


function url_shortcode() {
	return get_bloginfo('url');
}
add_shortcode('url','url_shortcode');




/*****************************************************************
//																//
// 						AJAX REQUEST       						//
//																//
*****************************************************************/

/**
 * Sample Ajax Function
 * note: you can rename this function
 *
 */
add_action('wp_ajax_submit_form_ajax', 'submit_form_ajax');
add_action('wp_ajax_nopriv_submit_form_ajax', 'submit_form_ajax');

function submit_form_ajax() {
	$post_data = $_POST;

	global $wpdb;

	try {
		
		/* verify query status */

		if($status) {
			$data['status'] 	  = 1;
			
			$result = serialize($data);
			throw new Exception($result);
		} else {
			$data['status']  = 0;
			$data['message'] = 'There an error occurred while processing your request. Please try again later';
			
			$result = serialize($data);
			throw new Exception($result);
		}

	} catch (Exception $e) {
		$result = unserialize($e->getMessage());
		if($result['message']) {
			$result['message'] = '<div class="alert alert-danger" role="alert"><p>'.$result['message'].'</p></div>';
		}
		wp_send_json($result);
		wp_die();
	}
}




/************************************************************************************************************************************************************************************
* 																										 	   																		*
*  																			 DASHBOARD CUSTOM FUNCTIONS 	   																		*
* 																										 	   																		*
*************************************************************************************************************************************************************************************/

/*****************************************************
//													//
//  				LOADS SUPPORT          			//
//													//
*****************************************************/

add_theme_support( 'post-thumbnails' );


/*****************************************************
//													//
//  			   TinyMCE CONFIGURATION            //
//													//
*****************************************************/

if ( ! function_exists( 'wpex_mce_buttons' ) ) {
	function wpex_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'fontselect' ); // Add Font Select
		array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
		// array_unshift( $buttons, 'media' ); // Add Media button
		return $buttons;
	}
}
add_filter( 'mce_buttons_2', 'wpex_mce_buttons' );

// Customize mce editor font sizes
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "1px 2px 3px 4px 5px 6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 33px 34px 35px 36px 37px 38px 39px 40px 41px 42px 43px 44px 45px 46px 47px 48px 49px 50px 51px 52px 53px 54px 55px 56px 57px 58px 59px 60px 61px 62px 63px 64px 65px 66px 67px 68px 69px 70px";
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );

// Add custom Fonts to the Fonts list
if ( ! function_exists( 'wpex_mce_google_fonts_array' ) ) {
	function wpex_mce_google_fonts_array( $initArray ) {
		$initArray['font_formats'] = 'Lato Bold Italic=lato-bold-italic; Lato Bold=lato-bold; Lato Light=lato-light; Lato Regular=lato-regular; Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats';
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_google_fonts_array' );

// IMPORT DASHBOARD CUSTOM CSS
add_editor_style( get_template_directory_uri().'/dist/css/dashboard-style.min.css' );

function add_my_media_button() {
	echo '<a href="#" id="insert-my-media" class="button">Add Gallery Slider</a>';
}
add_action('media_buttons', 'add_my_media_button', 10);

function include_media_button_js_file() {
	wp_enqueue_script('media_button', get_bloginfo('template_url').'/dist/js/dashboard-js.min.js', array('jquery'), '1.0', true);
}
add_action('wp_enqueue_media', 'include_media_button_js_file');




function show_log($string) {
	echo "<pre>";
	print_r($string);
	echo "</pre>";
}


// add_action( 'phpmailer_init', 'wpse8170_phpmailer_init' );
function wpse8170_phpmailer_init( PHPMailer $phpmailer ) {
	$phpmailer->isSMTP();
	$phpmailer->Host = '';
	$phpmailer->Port = 465; // could be different
	$phpmailer->Username = ''; // if required
	$phpmailer->Password = ''; // if required
	$phpmailer->SMTPAuth = true; // if required
	$phpmailer->SMTPSecure = 'ssl'; // enable if required, 'tls' is another possible value
}


function do_send_email($recipient = 'carlo@activamedia.com.ph', $cc = null, $bcc = null, $from = 'From: GS <carlo@activamedia.com.ph>', $subject = 'Email Subject', $message_content = null) {
	$subject 	= $subject;
	$to 		= $recipient;
	$cc 		= $cc;
	$bcc 		= $bcc;

	$header 	= $from." \r\n";
	$header 	.= "MIME-Version: 1.0 \r\n";
	$header 	.= "Content-Type: text/html; charset=ISO-8859-1 \r\n";
	if($cc != null) {
		$header 	.= "Cc: $cc \r\n";
	}
	if($bcc != null) {
		$header 	.= "Bcc: $bcc \r\n";
	}

	// Generate HTML email template
	$message_template = '<html>
							<body>
								<table width="600" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
									<tr>
										<td align="center" style="background: #3b3b3b; padding: 25px;">
										   <img src="'.get_header_image().'" style="height: 61px;" title="Asus" alt="Asus">
										</td>
									</tr>
									<tr>
										<td width="400" align="left" valign="top" style="padding: 15px; font-size: 14px; word-break: break-word; border: 1px solid #ccc;">
											'.$message_content.'
										</td>
									</tr>
								</table>
							</body>
						</html>';
	
	// Email sending
	$send_email = wp_mail($to, $subject, $message_template, $header);

	if($send_email) {
		return true;
	} else {
		return false;
	}
}