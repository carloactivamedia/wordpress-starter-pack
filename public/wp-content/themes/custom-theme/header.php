<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title><?php wp_title(''); ?></title>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- HEADER -->
<div id="header">

	<?php if ( has_nav_menu( 'primary' ) ) : ?>
		<div class="nav-header-wrapper">
			<div class="container nav-header">
				<nav class="navbar navbar-default navbar-sp">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php bloginfo('url') ?>">
							Brand
						</a>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<?php
							$menu = array(
								'menu' 				=> 'primary',
								'theme_location'  	=> 'primary',
								'depth'  			=> 2,
								'container'  		=> '',
								'menu_class'  		=> 'nav navbar-nav navbar-right',
								'fallback_cb'  		=> 'wp_bootstrap_navwalker::fallback',
								'walker'  			=> new wp_bootstrap_navwalker());
							wp_nav_menu($menu);
						?>
					</div>
					
				</nav>
			</div>
		</div>
	<?php endif; ?>

	Header

</div>