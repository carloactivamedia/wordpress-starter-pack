<?php get_header(); ?>

<?php get_template_part( 'template-parts/banner/page', 'banner' ); ?>

<div id="content">
	<div class="container">
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', 'single' );
		endwhile;
		?>
	</div>
</div>

<?php get_footer(); ?>
