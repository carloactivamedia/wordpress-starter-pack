$(function() {

	'use strict';
	
	/**************
	FORMS
	***************/
	$.validate({
		form : 'form[name="redeem-step-one"]',
		errorMessagePosition : 'top',
		scrollToTopOnError : false,
		validateOnBlur : false,
		onSuccess: function(form){
			var $form = form;
			$.ajax({
				type: 'post',
				url: wpajax.ajax_url,
				data: $($form).serialize()+'&action=submit_form_ajax',
				dataType: 'json',
				beforeSend: function(){
					$($form).find('input[type="submit"]').attr('disabled', 'disabled');
				},
				success: function(res){
					if(res.status == 1) {
						location.href = res.step_two_url;
					} else if (res.status == 0) {
						$('.form-notification').html(res.message);
					} else {
						$('.form-notification').html('');
					}
				},
				complete: function(){
					$($form).find('input[type="submit"]').removeAttr('disabled');
				}
			});
			return false;
		}
	});

	function get_url_parameter(name) {
		return decodeURI(
					(RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
				);
	}
});