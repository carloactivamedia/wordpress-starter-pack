<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wsp_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q/go[Ok1o7@Qw`1Zx|k^XGl#t X]8`nLXiZ.tq{_G:C>.YMJ?IkB1`qsIYRI:t-+');
define('SECURE_AUTH_KEY',  'a=G)Yi$XKg#0jA4Cj$1 ~e.;F37jc#-kF( p]325=8ylvD-lRJJ5Vl*WxOvmpHM&');
define('LOGGED_IN_KEY',    'AO>aW/+kJ]+C!2!MtNE-dRdPeAfy|_Uz:z{[if&lnd+hh47U|(z,V=E,9L9LksN~');
define('NONCE_KEY',        '<Dbg/pdbhxDT7Hi72HW[` )yXyc<7/b]NwD=TGPmCGXp_.>?<;C$L{r~6:=F@I{ ');
define('AUTH_SALT',        'a40-dvD~<{(,tOMG0J4[BAcm0cx194!Zj~@~k|Vyj(7{>97==KZ)p%)mvN/~r>`$');
define('SECURE_AUTH_SALT', '5PFg)vb$(Y4wsnP940>Y:s?AKVJV+4oy*:X1mdagU]]6c|>LEqqH3k>DsLRcs![=');
define('LOGGED_IN_SALT',   'YSpaoWm#CJH.V7NI[Q@Tv)Tb5(eQzpO{mF2OpwS&J{8;T;|oy@a}@3nA<ZB$Q &U');
define('NONCE_SALT',       '*hCpm|Dcx@G7nH=~`Bs)!$L@ou1efXw7(1INH[a)44tR3AIR.V}%f#b%G<L`V(;k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
